package com.example.TableLanguage.dto.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceDtoOutput {
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String partner_code;
    private float price;
    private String desription;
    private int created_by;
    private int updated_by;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;
}
