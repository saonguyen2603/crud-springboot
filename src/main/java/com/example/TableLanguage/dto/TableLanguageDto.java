package com.example.TableLanguage.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;


@Data
public class TableLanguageDto {
    private int id;
    private String flag;
    @Pattern(regexp = "^[a-zA-Z0-9 ]{0,20}+$", message = "Không chứa kí tự đặc biệt")
    private String code;
    @Max(value=250, message = "Tên không vượt quá 250 ký tự")
    private String name;
    private String description;
    private Timestamp updated_at;
    private Timestamp created_at;
    private Timestamp deleted_at;
}
