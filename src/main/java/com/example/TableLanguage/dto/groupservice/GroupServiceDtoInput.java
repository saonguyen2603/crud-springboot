package com.example.TableLanguage.dto.groupservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupServiceDtoInput {
    @Pattern(regexp = "^[a-zA-Z0-9 ]{0,20}+$", message = "Không chứa kí tự đặc biệt")
    private String code;
    @Max(value=250, message = "Tên không vượt quá 250 ký tự")
    private String name;
    private String alias;
    private String image_url;
    private String desription;
    private boolean is_active;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;
}
