package com.example.TableLanguage.dto.groupservice;

import com.example.TableLanguage.Entity.ServiceEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupServiceDtoOutput {
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String desription;
    private boolean is_active;
    private Timestamp deleted_at;
//    @JsonProperty("group_service_id")
    private List<ServiceEntity> serviceEntityList;
//    @OneToMany(mappedBy = "groupServiceDtoOutput", fetch = FetchType.LAZY, cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    @JsonManagedReference
//    private List<ServiceDtoOutput> serviceDtoOutputs;
}
