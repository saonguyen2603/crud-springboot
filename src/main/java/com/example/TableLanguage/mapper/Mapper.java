package com.example.TableLanguage.mapper;

import com.example.TableLanguage.Entity.LanguageEntity;
import com.example.TableLanguage.dto.TableLanguageDto;
import org.mapstruct.MappingTarget;

@org.mapstruct.Mapper
public interface Mapper {
    LanguageEntity createTableLanguage1(TableLanguageDto tableLanguageDto);
    void updateTableLanguage(@MappingTarget LanguageEntity languageEntity, TableLanguageDto tableLanguageDto);

    TableLanguageDto entityToOutputDto(LanguageEntity languageEntity);



}
