package com.example.TableLanguage.mapper;

import com.example.TableLanguage.Entity.GroupServicesEntity;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoInput;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoOutput;
import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;


@org.mapstruct.Mapper
public interface MapperGroupService {
    GroupServicesEntity createGroupServices(GroupServiceDtoInput groupServiceDtoInput);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateGroupServices(@MappingTarget GroupServicesEntity groupServicesEntity, GroupServiceDtoInput groupServiceDtoInput);

    GroupServiceDtoOutput createGroupServices1(GroupServicesEntity groupServicesEntity);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GroupServiceDtoOutput updateGroupServices1(@MappingTarget GroupServiceDtoOutput groupServiceDtoOutput,  GroupServicesEntity groupServicesEntity);
    List<GroupServiceDtoOutput> getAll(List<GroupServicesEntity> groupServicesEntity);
    GroupServiceDtoOutput getById(long id, GroupServicesEntity groupServicesEntity);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GroupServicesEntity mapDtoOutputToEntity(@MappingTarget GroupServicesEntity groupServicesEntity, GroupServiceDtoOutput groupServiceDtoOutput);
}
