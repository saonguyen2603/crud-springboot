package com.example.TableLanguage.mapper;


import com.example.TableLanguage.Entity.ServiceEntity;
import com.example.TableLanguage.dto.service.ServiceDtoInput;
import com.example.TableLanguage.dto.service.ServiceDtoOutput;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper
public interface MapperService {
    ServiceEntity createServices(ServiceDtoInput serviceDtoInput);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    ServiceEntity updateServices(@MappingTarget ServiceEntity serviceEntity, ServiceDtoInput serviceDtoInput);
    ServiceDtoOutput createServices1(ServiceEntity serviceEntity);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    ServiceDtoOutput updateServices1(@MappingTarget ServiceDtoOutput serviceDtoOutput, ServiceEntity serviceEntity);
    List<ServiceDtoOutput> getAll(List<ServiceEntity> serviceEntity);
    ServiceDtoOutput getById(long id, ServiceEntity serviceEntity);
}
