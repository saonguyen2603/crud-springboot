package com.example.TableLanguage.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "table_language")
@NoArgsConstructor
@AllArgsConstructor
public class LanguageEntity implements Serializable {
    private static final long serialVersionUID = -297553281792804396L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String flag;
    private String code;
    private String name;
    private String description;
    private int created_by;
    private int updated_by;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;

}
