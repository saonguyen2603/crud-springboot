package com.example.TableLanguage.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


@Data
@Entity
@Table(name = "group_services")
@NoArgsConstructor
@AllArgsConstructor
public class GroupServicesEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String desription;
    private boolean is_active;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;

//    @OneToMany(mappedBy = "groupServicesEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    @JsonManagedReference
//    private List<ServiceEntity> servicesEntityList;

}
