package com.example.TableLanguage.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Table(name = "services")
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ServiceEntity implements Serializable {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String partner_code;
    private float price;
    private String desription;
    private int created_by;
    private int updated_by;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_service_id", referencedColumnName = "id")
    @JsonBackReference
    private GroupServicesEntity groupServicesEntity;
}
