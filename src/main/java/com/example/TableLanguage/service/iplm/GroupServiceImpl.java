package com.example.TableLanguage.service.iplm;

import com.example.TableLanguage.Entity.GroupServicesEntity;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoInput;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoOutput;
import com.example.TableLanguage.mapper.MapperGroupService;
import com.example.TableLanguage.repository.GroupServicesRepository;
import com.example.TableLanguage.repository.ServiceRepository;
import com.example.TableLanguage.service.GroupService;
import com.example.TableLanguage.validator.ValidateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    private GroupServicesRepository groupServicesRepository;
    @Autowired
    private MapperGroupService mapperGroupService;

    @Autowired
    private ServiceRepository serviceRepository;



    @Override
        public GroupServiceDtoOutput createGroupService(GroupServiceDtoInput groupServiceDtoInput) {
            String groupServiceDtoInput1 = ValidateGroupService.changeAlias(groupServiceDtoInput.getAlias());
            groupServiceDtoInput.setAlias(groupServiceDtoInput1);
            groupServiceDtoInput.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            GroupServicesEntity groupServicesEntity = mapperGroupService.createGroupServices(groupServiceDtoInput);
            groupServicesRepository.save(groupServicesEntity);
        return mapperGroupService.createGroupServices1(groupServicesEntity);
    }

    @Override
    public List<GroupServiceDtoOutput> getGroupService() {
        return groupServicesRepository.findAll().stream().map(groupServicesEntity -> detail(groupServicesEntity.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupServicesEntity> getGroupServiceEntity() {
        return groupServicesRepository.findAll();
    }

    @Override
    public GroupServiceDtoOutput detail(Long id) {
        GroupServiceDtoOutput output = mapperGroupService.createGroupServices1(getGroupServiceById(id));
        output.setServiceEntityList(serviceRepository.findAllByGroupServicesEntity(id));
        return output;
    }

    @Override
    public GroupServicesEntity getGroupServiceById(Long id) {
            Optional<GroupServicesEntity> optional = groupServicesRepository.findById(id);
            return optional.get();
    }

    @Override
    public GroupServicesEntity updateGroupService(Long id, GroupServiceDtoInput groupServiceDtoInput) {
        String groupServiceDtoInput1 = ValidateGroupService.changeAlias(groupServiceDtoInput.getAlias());
        groupServiceDtoInput.setAlias(groupServiceDtoInput1);
        groupServiceDtoInput.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        GroupServicesEntity groupServicesEntity = getGroupServiceById(id);
        mapperGroupService.updateGroupServices(groupServicesEntity, groupServiceDtoInput);
        return groupServicesRepository.save(groupServicesEntity);
    }

    @Override
    public void deleteGroupService(Long id) {
        groupServicesRepository.deleteById(id);
    }

    @Override
    public GroupServicesEntity softDelete(Long id) {
        GroupServicesEntity groupServicesEntity = getGroupServiceById(id);
        groupServicesEntity.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        return groupServicesRepository.save(groupServicesEntity);
    }

    @Override
    public boolean isGroupServiceExist(Long id) {
        return groupServicesRepository.existsById(id);
    }

    @Override
    public boolean isExistsByCode(String code) {
        return groupServicesRepository.existsByCode(code);
    }
}
