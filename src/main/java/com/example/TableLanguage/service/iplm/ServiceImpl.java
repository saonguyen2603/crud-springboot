package com.example.TableLanguage.service.iplm;

import com.example.TableLanguage.Entity.GroupServicesEntity;
import com.example.TableLanguage.Entity.ServiceEntity;
import com.example.TableLanguage.dto.service.ServiceDtoInput;
import com.example.TableLanguage.dto.service.ServiceDtoOutput;
import com.example.TableLanguage.mapper.MapperService;
import com.example.TableLanguage.repository.GroupServicesRepository;
import com.example.TableLanguage.repository.ServiceRepository;
import com.example.TableLanguage.service.Service;
import com.example.TableLanguage.validator.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    GroupServicesRepository groupServicesRepository;

    @Autowired
    private MapperService mapperService;

    @Override
    public ServiceDtoOutput createService(ServiceDtoInput serviceDtoInput) {
        boolean check = ValidateService.checkPartnerCode(serviceDtoInput.getPartner_code());
        if (check) {
            serviceDtoInput.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            ServiceEntity serviceEntity = mapperService.createServices(serviceDtoInput);
            GroupServicesEntity groupServicesEntity = groupServicesRepository.findById(serviceDtoInput.getGroup_service_id())
                    .orElseThrow(() -> new RuntimeException("Không tìm thấy dữ liệu."));
            serviceEntity.setGroupServicesEntity(groupServicesEntity);
            serviceRepository.save(serviceEntity);
            return mapperService.createServices1(serviceEntity);
        } else {
            throw new RuntimeException("Partner_code ký tự đầu phải là chữ cái");
        }
    }

    @Override
    public List<ServiceDtoOutput> getService() {
        return serviceRepository.findAll().stream().map(mapperService::createServices1).collect(Collectors.toList());
    }

    @Override
    public ServiceEntity getServiceById(Long id) {
        Optional<ServiceEntity> optional = serviceRepository.findById(id);
        return optional.get();
    }

    @Override
    public ServiceEntity updateService(Long id, ServiceDtoInput serviceDtoInput) {
        boolean check = ValidateService.checkPartnerCode(serviceDtoInput.getPartner_code());
        if (check) {
            ServiceEntity serviceEntity = getServiceById(id);
            serviceDtoInput.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            GroupServicesEntity groupServicesEntity = groupServicesRepository.findById(serviceDtoInput.getGroup_service_id())
                    .orElseThrow(() -> new RuntimeException("Không tìm thấy dữ liệu."));
            serviceEntity.setGroupServicesEntity(groupServicesEntity);
            mapperService.updateServices(serviceEntity, serviceDtoInput);
            return serviceRepository.save(serviceEntity);
        } else{
            throw new RuntimeException("Partner_code ký tự đầu phải là chữ cái");
        }
    }

    @Override
    public void deleteService(Long id) {
        serviceRepository.deleteById(id);
    }

    @Override
    public ServiceEntity softDelete(Long id, ServiceDtoInput serviceDtoInput) {
        serviceDtoInput.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        ServiceEntity serviceEntity = getServiceById(id);
        mapperService.updateServices(serviceEntity, serviceDtoInput);
        return  serviceRepository.save(serviceEntity);
    }

    @Override
    public boolean isServiceExist(Long id) {
        return serviceRepository.existsById(id);
    }

    @Override
    public boolean isExistsByCode(String code) {
        return serviceRepository.existsByCode(code);
    }
}
