package com.example.TableLanguage.service.iplm;

import com.example.TableLanguage.Entity.LanguageEntity;
import com.example.TableLanguage.dto.TableLanguageDto;
import com.example.TableLanguage.mapper.Mapper;
import com.example.TableLanguage.repository.TableLanguagueRepository;
import com.example.TableLanguage.service.TableLanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TableLanguageServiceImpl implements TableLanguageService {
    @Autowired
    private TableLanguagueRepository tableLanguagueRepo;

    @Autowired
    private Mapper mapper;

    @Override
    public LanguageEntity createTableLanguage(TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        return tableLanguagueRepo.save(mapper.createTableLanguage1(tableLanguageDto));
    }


    @Override
    public LanguageEntity updateTableLanguage(int id, TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        LanguageEntity languageEntity = getTableLanguage(id);
        mapper.updateTableLanguage(languageEntity, tableLanguageDto);
        return tableLanguagueRepo.save(languageEntity);
    }

    @Override
    public LanguageEntity getTableLanguage(int id) {
        Optional<LanguageEntity> optional = tableLanguagueRepo.findById(id);

        return optional.get();
    }

    @Override
    public List<LanguageEntity> getTableLanguage() {
        return  tableLanguagueRepo.findAll();
    }

    @Override
    public void deleteTableLanguage(int id) {
        tableLanguagueRepo.deleteById(id);
    }

    @Override
    public LanguageEntity softDeleteTableLanguage(int id, TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        LanguageEntity languageEntity = getTableLanguage(id);
        mapper.updateTableLanguage(languageEntity, tableLanguageDto);
        return tableLanguagueRepo.save(languageEntity);
    }

    @Override
    public boolean isTableLanguageExist(int id) {
        return tableLanguagueRepo.existsById(id);
    }

    @Override
    public Boolean isExistByCode(String code) {
       return tableLanguagueRepo.existsByCode(code);
    }
}
