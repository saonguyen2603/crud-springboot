package com.example.TableLanguage.service;

import com.example.TableLanguage.Entity.GroupServicesEntity;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoInput;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoOutput;
import org.springframework.stereotype.Service;

import java.util.List;

import static liquibase.pro.license.keymgr.e.g;

@Service
public interface GroupService {
    GroupServiceDtoOutput createGroupService(GroupServiceDtoInput groupServiceDtoInput);

    List<GroupServiceDtoOutput> getGroupService();
    List<GroupServicesEntity> getGroupServiceEntity();

    GroupServiceDtoOutput detail(Long id);

    GroupServicesEntity getGroupServiceById(Long id);

    GroupServicesEntity updateGroupService(Long id, GroupServiceDtoInput groupServiceDtoInput);

    void deleteGroupService(Long id);

    GroupServicesEntity softDelete(Long id);

    boolean isGroupServiceExist(Long id);

    boolean isExistsByCode(String code);
}
