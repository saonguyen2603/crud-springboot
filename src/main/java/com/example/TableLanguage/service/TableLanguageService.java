package com.example.TableLanguage.service;


import com.example.TableLanguage.Entity.LanguageEntity;

import com.example.TableLanguage.dto.TableLanguageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TableLanguageService {

    LanguageEntity createTableLanguage(TableLanguageDto tableLanguageDto);

    LanguageEntity updateTableLanguage(int id, TableLanguageDto tableLanguageDto);

    LanguageEntity getTableLanguage(int id);

    List<LanguageEntity> getTableLanguage();

    void deleteTableLanguage(int id);

    LanguageEntity softDeleteTableLanguage(int id, TableLanguageDto tableLanguageDto);

    boolean isTableLanguageExist(int id);

    Boolean isExistByCode(String code);
}
