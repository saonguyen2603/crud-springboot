package com.example.TableLanguage.service;

import com.example.TableLanguage.Entity.ServiceEntity;
import com.example.TableLanguage.dto.service.ServiceDtoInput;
import com.example.TableLanguage.dto.service.ServiceDtoOutput;

import java.util.List;

@org.springframework.stereotype.Service
public interface Service {
    ServiceDtoOutput createService(ServiceDtoInput serviceDtoInput);
    List<ServiceDtoOutput> getService();
    ServiceEntity getServiceById(Long id);
    ServiceEntity updateService(Long id, ServiceDtoInput serviceDtoInput);
    void deleteService(Long id);
    ServiceEntity softDelete(Long id, ServiceDtoInput serviceDtoInput);
    boolean isServiceExist(Long id);
    boolean isExistsByCode(String code);
}
