package com.example.TableLanguage.controller;

import com.example.TableLanguage.Entity.GroupServicesEntity;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoInput;
import com.example.TableLanguage.dto.groupservice.GroupServiceDtoOutput;
import com.example.TableLanguage.exeption.ExceptionHandle;
import com.example.TableLanguage.exeption.InfoNotFoundException;
import com.example.TableLanguage.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/v1/groupservice")
public class GroupServiceController {
    @Autowired
    private GroupService groupService;

    @PostMapping
    public ResponseEntity<Object> createGroupService(@RequestBody GroupServiceDtoInput groupServiceDtoInput) {

        if (groupService.isExistsByCode(groupServiceDtoInput.getCode())) {
            throw new ExceptionHandle();
        } else {
            GroupServiceDtoOutput groupServiceDtoOutput = groupService.createGroupService(groupServiceDtoInput);
            return new ResponseEntity<>(groupServiceDtoOutput, HttpStatus.CREATED);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateGroupService(@PathVariable("id") Long id, @RequestBody GroupServiceDtoInput groupServiceDtoInput) {
        boolean isGroupServiceExist = groupService.isGroupServiceExist(id);
        if (isGroupServiceExist) {
            GroupServicesEntity groupServicesEntity = groupService.updateGroupService(id, groupServiceDtoInput);
            return ResponseEntity.ok().body(groupServicesEntity);
        } else {
            throw new InfoNotFoundException();
        }
    }

    @PutMapping("/softdelete/{id}")
    public ResponseEntity<Object> softDeleteGroupService(@PathVariable("id") Long id) {
        boolean isGroupServiceExist = groupService.isGroupServiceExist(id);
        if (isGroupServiceExist) {
            GroupServicesEntity groupServicesEntity1 = groupService.softDelete(id);
            return ResponseEntity.ok().body(groupServicesEntity1);
        } else {
            throw new InfoNotFoundException();
        }
    }

    @GetMapping
    public List<GroupServiceDtoOutput> getGroupService() {
        return groupService.getGroupService();
    }

    @GetMapping("/{id}")
    public GroupServiceDtoOutput getGroupServiceById(@PathVariable("id") Long id) {
        return groupService.detail(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteGroupService(@PathVariable("id") Long id) {
        boolean isGroupServiceExist = groupService.isGroupServiceExist(id);
        if (isGroupServiceExist) {
            groupService.deleteGroupService(id);
            return new ResponseEntity<>("Group Serviceis delete successfully.", HttpStatus.OK);
        } else {
            throw new InfoNotFoundException();
        }
    }
}
