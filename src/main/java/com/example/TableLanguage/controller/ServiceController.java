package com.example.TableLanguage.controller;

import com.example.TableLanguage.Entity.ServiceEntity;
import com.example.TableLanguage.dto.service.ServiceDtoInput;
import com.example.TableLanguage.dto.service.ServiceDtoOutput;
import com.example.TableLanguage.exeption.ExceptionHandle;
import com.example.TableLanguage.exeption.InfoNotFoundException;
import com.example.TableLanguage.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/service")
public class ServiceController {
    @Autowired
    private Service service;


    @PostMapping
    public ResponseEntity<Object> createService(@RequestBody ServiceDtoInput serviceDtoInput) {
        if (service.isExistsByCode(serviceDtoInput.getCode())) {
            throw new ExceptionHandle();
        } else {
            ServiceDtoOutput serviceDtoOutput = service.createService(serviceDtoInput);
            return new ResponseEntity<>(serviceDtoOutput, HttpStatus.CREATED);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateService(@PathVariable("id") Long id, @RequestBody ServiceDtoInput serviceDtoInput) {
        boolean isServiceExist = service.isServiceExist(id);
        if (isServiceExist) {
            ServiceEntity serviceEntity = service.updateService(id, serviceDtoInput);
            return ResponseEntity.ok().body(serviceEntity);
        } else {
            throw new InfoNotFoundException();
        }
    }

    @PutMapping("/softdelete/{id}")
    public ResponseEntity<Object> softDeleteService(@PathVariable("id") Long id, @RequestBody ServiceDtoInput serviceDtoInput) {
        boolean isServiceExist = service.isServiceExist(id);
        if (isServiceExist) {
            ServiceEntity serviceEntity = service.softDelete(id, serviceDtoInput);
            return ResponseEntity.ok().body(serviceEntity);
        } else {
            throw new InfoNotFoundException();
        }
    }

    @GetMapping
    public List<ServiceDtoOutput> getService() {
        return service.getService();
    }

    @GetMapping("/{id}")
    public ServiceEntity getServiceById(@PathVariable("id") Long id) {
        return service.getServiceById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteService(@PathVariable("id") Long id) {
        boolean isGServiceExist = service.isServiceExist(id);
        if (isGServiceExist) {
            service.deleteService(id);
            return new ResponseEntity<>("Group Serviceis delete successfully.", HttpStatus.OK);
        } else {
            throw new InfoNotFoundException();
        }
    }
}
