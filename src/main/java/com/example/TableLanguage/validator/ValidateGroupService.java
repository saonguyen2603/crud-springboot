package com.example.TableLanguage.validator;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class ValidateGroupService {
    public static String changeAlias(String alias) {
        alias = alias.trim();
        String checkok = alias.toLowerCase().replace(" ", "-");
        String temp = Normalizer.normalize(checkok, Normalizer.Form.NFD);

        temp = temp.replaceAll("đ","d");
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }
}
