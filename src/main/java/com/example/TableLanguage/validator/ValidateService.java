package com.example.TableLanguage.validator;

import java.util.regex.Pattern;

public class ValidateService {
    public static Boolean checkPartnerCode(String partnerCode){
        String a = partnerCode.substring(0,1);
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        if (pattern.matcher(a).find()) {
            return true;
        }else {
            return false;
        }
    }
}
