package com.example.TableLanguage.repository;


import com.example.TableLanguage.Entity.GroupServicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface GroupServicesRepository extends JpaRepository<GroupServicesEntity, Long> {
    boolean existsByCode(String code);
}
