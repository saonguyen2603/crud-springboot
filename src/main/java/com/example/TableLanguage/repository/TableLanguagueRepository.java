package com.example.TableLanguage.repository;

import com.example.TableLanguage.Entity.LanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TableLanguagueRepository extends JpaRepository<LanguageEntity, Integer> {
    boolean existsByCode(String code);
}
