package com.example.TableLanguage.repository;

import com.example.TableLanguage.Entity.ServiceEntity;
import com.example.TableLanguage.dto.service.ServiceDtoOutput;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity, Long> {
    boolean existsByCode(String code);

    @Query(value = "SELECT * from services where group_service_id = :id",nativeQuery = true)
    List<ServiceEntity> findAllByGroupServicesEntity(@Param("id") Long id);
}
