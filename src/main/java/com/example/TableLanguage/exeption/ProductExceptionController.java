package com.example.TableLanguage.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductExceptionController {
    @ExceptionHandler(value = ExceptionHandle.class)
    public ResponseEntity<Object> exception(ExceptionHandle exception) {
        return new ResponseEntity<>("Mã code đã trùng, vui lòng nhập lại", HttpStatus.MULTI_STATUS);
    }

    @ExceptionHandler(value = InfoNotFoundException.class)
    public ResponseEntity<Object> exception1(ExceptionHandle exception) {
        return new ResponseEntity<>("Id không tồn tại.", HttpStatus.MULTI_STATUS);
    }
}